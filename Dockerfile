# Node.js
FROM node:16-alpine as node

# ENV PRISMA_QUERY_ENGINE_LIBRARY=/usr/src/app/node_modules/.prisma/client/libquery_engine-darwin-arm64.dylib.node

WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn install

COPY . .

# Copy the scripts directory
COPY scripts/ /usr/src/app/scripts

RUN wget -O /usr/src/app/scripts/wait-for-it.sh https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh && chmod +x /usr/src/app/scripts/wait-for-it.sh

RUN npx prisma generate

CMD yarn migrate && yarn start
