import { IsNotEmpty } from 'class-validator';
import { __ } from '../../../helpers/functions';

export class AboutMeBaseInputDto {
  @IsNotEmpty({
    message: () => __('Title is required'),
  })
  title: string;
  description?: string;
  name?: string;
  designation?: string;
  email?: string;
  secondary_email?: string;
  phone?: string;
  secondary_phone?: string;
  address?: string;
  secondary_address?: string;
  nationality?: string;
  language?: string;
  secondary_language?: string;
  image?: string;
}

export class CreateAboutMeInputDto extends AboutMeBaseInputDto {}
export class UpdateAboutMeInputDto extends AboutMeBaseInputDto {
  @IsNotEmpty({
    message: () => __('Uid is required'),
  })
  uid: string;
}
