import { Module } from '@nestjs/common';
import { AboutMeService } from './about_me.service';
import { AboutMeController } from './about_me.controller';

@Module({
  imports: [],
  controllers: [AboutMeController],
  providers: [AboutMeService],
  exports: [],
})
export class AboutMeModule {}
