import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../../libs/auth/auth.gaurd';
import { ResponseModel } from '../../models/custom/common.response.model';
import { AboutMe } from '../../models/db/about_me.model';
import { AboutMeService } from './about_me.service';
import { CreateAboutMeInputDto, UpdateAboutMeInputDto } from './dto/input.dto';
import { Request } from 'express';
@UseGuards(JwtAuthGuard())
@Controller('about-me')
export class AboutMeController {
  constructor(private readonly service: AboutMeService) {}

  @Get(':uid')
  async aboutMe(@Param() params: any): Promise<AboutMe | null> {
    return await this.service.aboutMe(params.uid);
  }

  @Post('create')
  async create(
    @Req() req: Request,
    @Body() data: CreateAboutMeInputDto,
  ): Promise<ResponseModel> {
    return this.service.create(req, data);
  }

  @Post('update')
  async update(
    @Req() req: Request,
    @Body() data: UpdateAboutMeInputDto,
  ): Promise<ResponseModel> {
    return this.service.update(req, data);
  }
}
