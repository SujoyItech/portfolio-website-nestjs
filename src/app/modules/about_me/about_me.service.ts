import { CreateAboutMeInputDto, UpdateAboutMeInputDto } from './dto/input.dto';
import {
  __,
  deleteImage,
  prisma_client,
  processException,
  successResponse,
  uploadImage,
} from '../../helpers/functions';
import { ResponseModel } from '../../models/custom/common.response.model';
import { AboutMe } from '../../models/db/about_me.model';
import { throwNewError } from '../../exception/exception_filter';
import { Request } from 'express';
export class AboutMeService {
  async aboutMe(uid: string): Promise<AboutMe | null> {
    try {
      return await prisma_client.aboutMe.findFirst({
        where: {
          uid: uid,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async create(
    req: Request,
    data: CreateAboutMeInputDto,
  ): Promise<ResponseModel> {
    try {
      const files = req.files as Express.Multer.File[];
      if (files && files.length > 0) {
        for (const file of files) {
          if ((file.fieldname = 'image')) {
            const imageUrl = await uploadImage(file, `profile`);
            data.image = imageUrl;
          }
        }
      }
      const about_me = await prisma_client.aboutMe.create({
        data: data,
      });
      return successResponse(__('Created successfully!'), about_me);
    } catch (e) {
      processException(e);
    }
  }

  async update(
    req: Request,
    data: UpdateAboutMeInputDto,
  ): Promise<ResponseModel> {
    try {
      const { uid } = data;
      const about_me = await prisma_client.aboutMe.findFirst({
        where: {
          uid: uid,
        },
      });
      if (!about_me) {
        throwNewError('Invalid uid!');
      }
      let image = about_me.image;

      const files = req.files as Express.Multer.File[];
      if (files.length > 0) {
        for (const file of files) {
          if ((file.fieldname = 'image')) {
            const imageUrl = await uploadImage(file, `about-me`);
            if (imageUrl) {
              image = imageUrl;
              await deleteImage(about_me.image);
            }
          }
        }
      }

      data.image = image;
      const update = await prisma_client.aboutMe.update({
        where: {
          uid: uid,
        },
        data: data,
      });
      return successResponse(__('Updated successfully!'), update);
    } catch (e) {
      processException(e);
    }
  }
}
