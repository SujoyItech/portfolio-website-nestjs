import {
  CreateSkillCategoryDto,
  UpdateSkillCategoryDto,
} from '../dto/input.dto';
import {
  __,
  prisma_client,
  processException,
  successResponse,
} from '../../../helpers/functions';
import { ResponseModel } from '../../../models/custom/common.response.model';
import { SkillCategory } from '../../../models/db/skill_category.model';
import { throwNewError } from '../../../exception/exception_filter';
import { STATUS_ACTIVE } from '../../../helpers/coreconstant';

export class SkillCategoryService {
  async findMany(): Promise<SkillCategory[]> {
    try {
      return await prisma_client.skillCategory.findMany({
        where: {
          status: STATUS_ACTIVE,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async findOne(id: string): Promise<SkillCategory | null> {
    try {
      return await prisma_client.skillCategory.findFirst({
        where: {
          uid: id,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async create(
    createSkillCategoryDto: CreateSkillCategoryDto,
  ): Promise<ResponseModel> {
    try {
      await prisma_client.skillCategory.create({
        data: createSkillCategoryDto,
      });
      return successResponse(__('Created successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async update(
    uid: string,
    updateSkillCategoryDto: UpdateSkillCategoryDto,
  ): Promise<ResponseModel> {
    try {
      const skillCategory = await prisma_client.skillCategory.findFirst({
        where: {
          uid: uid,
        },
      });
      if (!skillCategory) {
        throwNewError('Invalid uid!');
      }

      await prisma_client.skillCategory.update({
        where: {
          id: skillCategory.id,
        },
        data: updateSkillCategoryDto,
      });
      return successResponse(__('Updated successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async delete(id: string): Promise<ResponseModel> {
    try {
      const skillCategory = await prisma_client.skillCategory.findFirst({
        where: {
          uid: id,
        },
      });
      if (!skillCategory) {
        throwNewError('Invalid id!');
      }

      await prisma_client.skillCategory.delete({
        where: {
          id: skillCategory.id,
        },
      });
      return successResponse(__('Deleted successfully!'));
    } catch (e) {
      processException(e);
    }
  }
}
