import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../../../libs/auth/auth.gaurd';
import {
  CreateSkillCategoryDto,
  UpdateSkillCategoryDto,
} from '../dto/input.dto';
import { SkillCategoryService } from './category.service';
import { ResponseModel } from '../../../models/custom/common.response.model';
import { SkillCategory } from '../../../models/db/skill_category.model';

@UseGuards(JwtAuthGuard())
@Controller('skill-category')
export class SkillCategoryController {
  constructor(private readonly service: SkillCategoryService) {}

  @Get('')
  async findMany(): Promise<SkillCategory[]> {
    return await this.service.findMany();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<SkillCategory | null> {
    return await this.service.findOne(id);
  }

  @Post()
  async create(@Body() data: CreateSkillCategoryDto): Promise<ResponseModel> {
    return this.service.create(data);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() data: UpdateSkillCategoryDto,
  ): Promise<ResponseModel> {
    return this.service.update(id, data);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<ResponseModel> {
    return this.service.delete(id);
  }
}
