import { IsNotEmpty } from 'class-validator';
import { __ } from '../../../helpers/functions';

export class SkillCategoryBaseDto {
  @IsNotEmpty({
    message: () => __('Title is required'),
  })
  title: string;
  status: number;
}

export class CreateSkillCategoryDto extends SkillCategoryBaseDto {}
export class UpdateSkillCategoryDto extends SkillCategoryBaseDto {}

export class SkillBaseDto {
  @IsNotEmpty({
    message: () => __('Title is required'),
  })
  title: string;
  description?: string;
  organization?: string;
  date_range?: string;
  percent?: number;
  status?: number;
}

export class CreateSkillDto extends SkillBaseDto {}
export class UpdateSkillDto extends SkillBaseDto {}
