import { CreateSkillDto, UpdateSkillDto } from './dto/input.dto';
import {
  __,
  prisma_client,
  processException,
  successResponse,
} from '../../helpers/functions';
import { ResponseModel } from '../../models/custom/common.response.model';
import { Skill } from '../../models/db/skill.model';
import { throwNewError } from '../../exception/exception_filter';
import { STATUS_ACTIVE } from '../../helpers/coreconstant';

export class SkillService {
  async findMany(): Promise<Skill[]> {
    try {
      return await prisma_client.skill.findMany({
        where: {
          status: STATUS_ACTIVE,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async findOne(uid: string): Promise<Skill | null> {
    try {
      return await prisma_client.skill.findFirst({
        where: {
          uid: uid,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async create(createSkillDto: CreateSkillDto): Promise<ResponseModel> {
    try {
      await prisma_client.skill.create({
        data: createSkillDto,
      });
      return successResponse(__('Created successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async update(
    id: string,
    updateSkillDto: UpdateSkillDto,
  ): Promise<ResponseModel> {
    try {
      const skill = await prisma_client.skill.findFirst({
        where: {
          uid: id,
        },
      });
      if (!skill) {
        throwNewError('Invalid skill id');
      }

      await prisma_client.skill.update({
        where: {
          uid: id,
        },
        data: updateSkillDto,
      });
      return successResponse(__('Updated successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async delete(id: string): Promise<ResponseModel> {
    try {
      const skill = await prisma_client.skill.findFirst({
        where: {
          uid: id,
        },
      });
      if (!skill) {
        throwNewError('Invalid skill id');
      }

      await prisma_client.skill.delete({
        where: {
          uid: id,
        },
      });
      return successResponse(__('Deleted successfully!'));
    } catch (e) {
      processException(e);
    }
  }
}
