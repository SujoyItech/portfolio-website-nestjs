import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../../libs/auth/auth.gaurd';
import { ResponseModel } from '../../models/custom/common.response.model';
import { Skill } from '../../models/db/skill.model';
import { CreateSkillDto, UpdateSkillDto } from './dto/input.dto';
import { SkillService } from './skills.service';

@UseGuards(JwtAuthGuard())
@Controller('skill')
export class SkillController {
  constructor(private readonly service: SkillService) {}

  @Get()
  async findAll(): Promise<Skill[]> {
    return await this.service.findMany();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Skill | null> {
    return await this.service.findOne(id);
  }

  @Post()
  async create(@Body() createSkillDto: CreateSkillDto): Promise<ResponseModel> {
    return await this.service.create(createSkillDto);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateSkillDto: UpdateSkillDto,
  ): Promise<ResponseModel> {
    return await this.service.update(id, updateSkillDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<ResponseModel> {
    return await this.service.delete(id);
  }
}
