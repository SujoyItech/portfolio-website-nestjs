import { Module } from '@nestjs/common';
import { SkillController } from './skills.controller';
import { SkillService } from './skills.service';
import { SkillCategoryController } from './category/category.controller';
import { SkillCategoryService } from './category/category.service';

@Module({
  imports: [],
  controllers: [SkillCategoryController, SkillController],
  providers: [SkillCategoryService, SkillService],
  exports: [],
})
export class SkillModule {}
