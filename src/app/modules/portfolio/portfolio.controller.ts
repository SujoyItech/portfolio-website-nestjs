import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../../libs/auth/auth.gaurd';
import { ResponseModel } from '../../models/custom/common.response.model';
import { Portfolio } from '../../models/db/portfolio.model';
import { CreatePortfolioDto, UpdatePortfolioDto } from './dto/input.dto';
import { PortfolioService } from './portfolio.service';

@UseGuards(JwtAuthGuard())
@Controller('skill')
export class PortfolioController {
  constructor(private readonly service: PortfolioService) {}

  @Get()
  async findAll(): Promise<Portfolio[]> {
    return await this.service.findMany();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Portfolio | null> {
    return await this.service.findOne(id);
  }

  @Post()
  async create(
    @Body() createPortfolioDto: CreatePortfolioDto,
  ): Promise<ResponseModel> {
    return await this.service.create(createPortfolioDto);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updatePortfolioDto: UpdatePortfolioDto,
  ): Promise<ResponseModel> {
    return await this.service.update(id, updatePortfolioDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<ResponseModel> {
    return await this.service.delete(id);
  }
}
