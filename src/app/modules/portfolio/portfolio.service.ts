import { CreatePortfolioDto, UpdatePortfolioDto } from './dto/input.dto';
import {
  __,
  prisma_client,
  processException,
  successResponse,
} from '../../helpers/functions';
import { ResponseModel } from '../../models/custom/common.response.model';
import { Portfolio } from '../../models/db/portfolio.model';
import { throwNewError } from '../../exception/exception_filter';
import { STATUS_ACTIVE } from '../../helpers/coreconstant';

export class PortfolioService {
  async findMany(): Promise<Portfolio[]> {
    try {
      return await prisma_client.portfolio.findMany({
        where: {
          status: STATUS_ACTIVE,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async findOne(uid: string): Promise<Portfolio | null> {
    try {
      return await prisma_client.portfolio.findFirst({
        where: {
          uid: uid,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async create(createPortfolioDto: CreatePortfolioDto): Promise<ResponseModel> {
    try {
      await prisma_client.portfolio.create({
        data: createPortfolioDto,
      });
      return successResponse(__('Created successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async update(
    id: string,
    updatePortfolioDto: UpdatePortfolioDto,
  ): Promise<ResponseModel> {
    try {
      const portfolio = await prisma_client.portfolio.findFirst({
        where: {
          uid: id,
        },
      });
      if (!portfolio) {
        throwNewError('Invalid portfolio id');
      }

      await prisma_client.portfolio.update({
        where: {
          uid: id,
        },
        data: updatePortfolioDto,
      });
      return successResponse(__('Updated successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async delete(id: string): Promise<ResponseModel> {
    try {
      const portfolio = await prisma_client.portfolio.findFirst({
        where: {
          uid: id,
        },
      });
      if (!portfolio) {
        throwNewError('Invalid portfolio id');
      }

      await prisma_client.portfolio.delete({
        where: {
          uid: id,
        },
      });
      return successResponse(__('Deleted successfully!'));
    } catch (e) {
      processException(e);
    }
  }
}
