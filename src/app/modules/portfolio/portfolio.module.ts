import { Module } from '@nestjs/common';
import { PortfolioCategoryController } from './category/category.controller';
import { PortfolioCategoryService } from './category/category.service';
import { PortfolioController } from './portfolio.controller';
import { PortfolioService } from './portfolio.service';

@Module({
  imports: [],
  controllers: [PortfolioCategoryController, PortfolioController],
  providers: [PortfolioCategoryService, PortfolioService],
  exports: [],
})
export class PortfolioModule {}
