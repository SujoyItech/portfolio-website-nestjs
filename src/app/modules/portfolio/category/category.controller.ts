import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../../../libs/auth/auth.gaurd';
import {
  CreatePortfolioCategoryDto,
  UpdatePortfolioCategoryDto,
} from '../dto/input.dto';
import { PortfolioCategoryService } from './category.service';
import { ResponseModel } from '../../../models/custom/common.response.model';
import { PortfolioCategory } from '../../../models/db/portfolio_category.model';

@UseGuards(JwtAuthGuard())
@Controller('portfolio-category')
export class PortfolioCategoryController {
  constructor(private readonly service: PortfolioCategoryService) {}

  @Get('')
  async findMany(): Promise<PortfolioCategory[]> {
    return await this.service.findMany();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<PortfolioCategory | null> {
    return await this.service.findOne(id);
  }

  @Post()
  async create(
    @Body() createPortfolioCategoryDto: CreatePortfolioCategoryDto,
  ): Promise<ResponseModel> {
    return this.service.create(createPortfolioCategoryDto);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updatePortfolioCategoryDto: UpdatePortfolioCategoryDto,
  ): Promise<ResponseModel> {
    return this.service.update(id, updatePortfolioCategoryDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<ResponseModel> {
    return this.service.delete(id);
  }
}
