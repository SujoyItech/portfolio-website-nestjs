import {
  CreatePortfolioCategoryDto,
  UpdatePortfolioCategoryDto,
} from '../dto/input.dto';
import {
  __,
  prisma_client,
  processException,
  successResponse,
} from '../../../helpers/functions';
import { ResponseModel } from '../../../models/custom/common.response.model';
import { PortfolioCategory } from '../../../models/db/portfolio_category.model';
import { throwNewError } from '../../../exception/exception_filter';
import { STATUS_ACTIVE } from '../../../helpers/coreconstant';

export class PortfolioCategoryService {
  async findMany(): Promise<PortfolioCategory[]> {
    try {
      return await prisma_client.portfolioCategory.findMany({
        where: {
          status: STATUS_ACTIVE,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async findOne(id: string): Promise<PortfolioCategory | null> {
    try {
      return await prisma_client.portfolioCategory.findFirst({
        where: {
          uid: id,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async create(
    createPortfolioCategoryDto: CreatePortfolioCategoryDto,
  ): Promise<ResponseModel> {
    try {
      await prisma_client.portfolioCategory.create({
        data: createPortfolioCategoryDto,
      });
      return successResponse(__('Created successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async update(
    uid: string,
    updatePortfolioCategoryDto: UpdatePortfolioCategoryDto,
  ): Promise<ResponseModel> {
    try {
      const portfolioCategory = await prisma_client.portfolioCategory.findFirst(
        {
          where: {
            uid: uid,
          },
        },
      );
      if (!portfolioCategory) {
        throwNewError('Invalid uid!');
      }

      await prisma_client.portfolioCategory.update({
        where: {
          id: portfolioCategory.id,
        },
        data: updatePortfolioCategoryDto,
      });
      return successResponse(__('Updated successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async delete(id: string): Promise<ResponseModel> {
    try {
      const portfolioCategory = await prisma_client.portfolioCategory.findFirst(
        {
          where: {
            uid: id,
          },
        },
      );
      if (!portfolioCategory) {
        throwNewError('Invalid id!');
      }

      await prisma_client.portfolioCategory.delete({
        where: {
          id: portfolioCategory.id,
        },
      });
      return successResponse(__('Deleted successfully!'));
    } catch (e) {
      processException(e);
    }
  }
}
