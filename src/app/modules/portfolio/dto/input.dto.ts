import { IsNotEmpty } from 'class-validator';
import { __ } from '../../../helpers/functions';

export class PortfolioCategoryBaseDto {
  @IsNotEmpty({
    message: () => __('Title is required'),
  })
  title: string;
  status: number;
}

export class CreatePortfolioCategoryDto extends PortfolioCategoryBaseDto {}
export class UpdatePortfolioCategoryDto extends PortfolioCategoryBaseDto {}

export class PortfolioBaseDto {
  @IsNotEmpty({
    message: () => __('Title is required'),
  })
  title: string;
  category_id: number;
  sub_title?: string;
  description?: string;
  project_link?: string;
  image?: string;
  status?: number;
}

export class CreatePortfolioDto extends PortfolioBaseDto {}
export class UpdatePortfolioDto extends PortfolioBaseDto {}
