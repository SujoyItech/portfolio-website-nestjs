import { CreateAchievementDto, UpdateAchievementDto } from './dto/input.dto';
import {
  __,
  deleteImage,
  prisma_client,
  processException,
  successResponse,
  uploadImage,
} from '../../helpers/functions';
import { ResponseModel } from '../../models/custom/common.response.model';
import { Achievement } from '../../models/db/achievement.model';
import { throwNewError } from '../../exception/exception_filter';
import { STATUS_ACTIVE } from '../../helpers/coreconstant';
import { Request } from 'express';

export class AchievementService {
  async findMany(): Promise<Achievement[]> {
    try {
      return await prisma_client.achievement.findMany({
        where: {
          status: STATUS_ACTIVE,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async findOne(uid: string): Promise<Achievement | null> {
    try {
      return await prisma_client.achievement.findFirst({
        where: {
          uid: uid,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async create(
    req: Request,
    createAchievementDto: CreateAchievementDto,
  ): Promise<ResponseModel> {
    try {
      const files = req.files as Express.Multer.File[];
      if (files && files.length > 0) {
        for (const file of files) {
          if ((file.fieldname = 'logo')) {
            const imageUrl = await uploadImage(file, `achievement`);
            createAchievementDto.logo = imageUrl;
          }
        }
      }
      await prisma_client.achievement.create({
        data: createAchievementDto,
      });
      return successResponse(__('Created successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async update(
    id: string,
    req: Request,
    updateAchievementDto: UpdateAchievementDto,
  ): Promise<ResponseModel> {
    try {
      const achievement = await prisma_client.achievement.findFirst({
        where: {
          uid: id,
        },
      });
      if (!achievement) {
        throwNewError('Invalid achievement id');
      }

      let logo = achievement.logo;

      const files = req.files as Express.Multer.File[];
      if (files.length > 0) {
        for (const file of files) {
          if ((file.fieldname = 'logo')) {
            const imageUrl = await uploadImage(file, `logo`);
            if (imageUrl) {
              logo = imageUrl;
              await deleteImage(achievement.logo);
            }
          }
        }
      }

      updateAchievementDto.logo = logo;

      await prisma_client.achievement.update({
        where: {
          uid: id,
        },
        data: updateAchievementDto,
      });
      return successResponse(__('Updated successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async delete(id: string): Promise<ResponseModel> {
    try {
      const achievement = await prisma_client.achievement.findFirst({
        where: {
          uid: id,
        },
      });
      if (!achievement) {
        throwNewError('Invalid achievement id');
      }

      await prisma_client.achievement.delete({
        where: {
          uid: id,
        },
      });
      return successResponse(__('Deleted successfully!'));
    } catch (e) {
      processException(e);
    }
  }
}
