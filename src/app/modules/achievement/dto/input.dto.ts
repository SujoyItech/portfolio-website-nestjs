import { IsNotEmpty } from 'class-validator';
import { __ } from '../../../helpers/functions';

export class AchievementBaseDto {
  @IsNotEmpty({
    message: () => __('Title is required'),
  })
  title: string;
  sub_title?: string;
  logo?: string;
  total_count?: number;
  status?: number;
}

export class CreateAchievementDto extends AchievementBaseDto {}
export class UpdateAchievementDto extends AchievementBaseDto {}
