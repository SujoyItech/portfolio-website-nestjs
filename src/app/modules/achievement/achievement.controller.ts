import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../../libs/auth/auth.gaurd';
import { ResponseModel } from '../../models/custom/common.response.model';
import { Achievement } from '../../models/db/achievement.model';
import { AchievementService } from './achievement.service';
import { CreateAchievementDto, UpdateAchievementDto } from './dto/input.dto';
import { Request } from 'express';

@UseGuards(JwtAuthGuard())
@Controller('achievement')
export class AchievementController {
  constructor(private readonly service: AchievementService) {}

  @Get()
  async findAll(): Promise<Achievement[]> {
    return await this.service.findMany();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Achievement | null> {
    return await this.service.findOne(id);
  }

  @Post()
  async create(
    @Req() req: Request,
    @Body() createAchievementDto: CreateAchievementDto,
  ): Promise<ResponseModel> {
    return await this.service.create(req, createAchievementDto);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Req() req: Request,
    @Body() updateAchievementDto: UpdateAchievementDto,
  ): Promise<ResponseModel> {
    return await this.service.update(id, req, updateAchievementDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<ResponseModel> {
    return await this.service.delete(id);
  }
}
