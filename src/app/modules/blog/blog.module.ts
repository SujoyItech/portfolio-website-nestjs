import { Module } from '@nestjs/common';
import { BlogService } from './blog.service';
import { BlogCategoryController } from './category/category.controller';
import { BlogCategoryService } from './category/category.service';
import { BlogController } from './blog.controller';
import { BlogTagController } from './tags/tag.controller';
import { BlogTagService } from './tags/tag.service';

@Module({
  imports: [],
  controllers: [BlogCategoryController, BlogTagController, BlogController],
  providers: [BlogCategoryService, BlogTagService, BlogService],
  exports: [],
})
export class BlogModule {}
