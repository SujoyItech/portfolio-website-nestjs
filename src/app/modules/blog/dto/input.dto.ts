import { IsInt, IsNotEmpty } from 'class-validator';
import { __ } from '../../../helpers/functions';

export class BlogCategoryBaseDto {
  @IsNotEmpty({
    message: () => __('Name is required'),
  })
  name: string;
  status: number;
}

export class CreateBlogCategoryDto extends BlogCategoryBaseDto {}
export class UpdateBlogCategoryDto extends BlogCategoryBaseDto {}

export class BlogTagBaseDto {
  @IsNotEmpty({
    message: () => __('Name is required'),
  })
  name: string;
  status: number;
}

export class CreateBlogTagDto extends BlogTagBaseDto {}
export class UpdateBlogTagDto extends BlogTagBaseDto {}

export class BlogBaseDto {
  @IsNotEmpty({
    message: () => __('Title is required'),
  })
  title: string;
  @IsNotEmpty({
    message: () => __('Slug is required'),
  })
  slug: string;
  @IsNotEmpty({
    message: () => __('Category is required'),
  })
  category_id: number;
  sub_title?: string;
  description?: string;
  project_link?: string;
  image?: string;
  status?: number;
}

export class CreateBlogDto extends BlogBaseDto {}
export class UpdateBlogDto extends BlogBaseDto {}
