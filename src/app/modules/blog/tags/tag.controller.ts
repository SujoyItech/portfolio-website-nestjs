import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../../../libs/auth/auth.gaurd';
import { CreateBlogTagDto, UpdateBlogTagDto } from '../dto/input.dto';
import { BlogTagService } from './tag.service';
import { ResponseModel } from '../../../models/custom/common.response.model';
import { BlogTag } from '../../../models/db/blog_tags.model';

@UseGuards(JwtAuthGuard())
@Controller('blog-tag')
export class BlogTagController {
  constructor(private readonly service: BlogTagService) {}

  @Get('')
  async findMany(): Promise<BlogTag[]> {
    return await this.service.findMany();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<BlogTag | null> {
    return await this.service.findOne(id);
  }

  @Post()
  async create(
    @Body() createBlogTagDto: CreateBlogTagDto,
  ): Promise<ResponseModel> {
    return this.service.create(createBlogTagDto);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateBlogTagDto: UpdateBlogTagDto,
  ): Promise<ResponseModel> {
    return this.service.update(id, updateBlogTagDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<ResponseModel> {
    return this.service.delete(id);
  }
}
