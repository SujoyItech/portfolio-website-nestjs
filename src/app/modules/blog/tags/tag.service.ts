import { CreateBlogTagDto, UpdateBlogTagDto } from '../dto/input.dto';
import {
  __,
  prisma_client,
  processException,
  successResponse,
} from '../../../helpers/functions';
import { ResponseModel } from '../../../models/custom/common.response.model';
import { BlogTag } from '../../../models/db/blog_tags.model';
import { throwNewError } from '../../../exception/exception_filter';
import { STATUS_ACTIVE } from '../../../helpers/coreconstant';

export class BlogTagService {
  async findMany(): Promise<BlogTag[]> {
    try {
      return await prisma_client.blogTag.findMany({
        where: {
          status: STATUS_ACTIVE,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async findOne(id: string): Promise<BlogTag | null> {
    try {
      return await prisma_client.blogTag.findFirst({
        where: {
          uid: id,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async create(createBlogTagDto: CreateBlogTagDto): Promise<ResponseModel> {
    try {
      await prisma_client.blogTag.create({
        data: createBlogTagDto,
      });
      return successResponse(__('Created successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async update(
    uid: string,
    updateBlogTagDto: UpdateBlogTagDto,
  ): Promise<ResponseModel> {
    try {
      const blogTag = await prisma_client.blogTag.findFirst({
        where: {
          uid: uid,
        },
      });
      if (!blogTag) {
        throwNewError('Invalid uid!');
      }

      await prisma_client.blogTag.update({
        where: {
          id: blogTag.id,
        },
        data: updateBlogTagDto,
      });
      return successResponse(__('Updated successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async delete(id: string): Promise<ResponseModel> {
    try {
      const blogTag = await prisma_client.blogTag.findFirst({
        where: {
          uid: id,
        },
      });
      if (!blogTag) {
        throwNewError('Invalid id!');
      }

      await prisma_client.blogTag.delete({
        where: {
          id: blogTag.id,
        },
      });
      return successResponse(__('Deleted successfully!'));
    } catch (e) {
      processException(e);
    }
  }
}
