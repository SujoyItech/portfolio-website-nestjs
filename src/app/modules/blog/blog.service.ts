import { CreateBlogDto, UpdateBlogDto } from './dto/input.dto';
import {
  __,
  prisma_client,
  processException,
  successResponse,
} from '../../helpers/functions';
import { ResponseModel } from '../../models/custom/common.response.model';
import { Blog } from '../../models/db/blog.model';
import { throwNewError } from '../../exception/exception_filter';
import { STATUS_ACTIVE } from '../../helpers/coreconstant';

export class BlogService {
  async findMany(): Promise<Blog[]> {
    try {
      return await prisma_client.blog.findMany({
        where: {
          status: STATUS_ACTIVE,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async findOne(uid: string): Promise<Blog | null> {
    try {
      return await prisma_client.blog.findFirst({
        where: {
          uid: uid,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async create(createBlogDto: CreateBlogDto): Promise<ResponseModel> {
    try {
      await prisma_client.blog.create({
        data: createBlogDto,
      });
      return successResponse(__('Created successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async update(
    id: string,
    updateBlogDto: UpdateBlogDto,
  ): Promise<ResponseModel> {
    try {
      const blog = await prisma_client.blog.findFirst({
        where: {
          uid: id,
        },
      });
      if (!blog) {
        throwNewError('Invalid blog id');
      }

      await prisma_client.blog.update({
        where: {
          uid: id,
        },
        data: updateBlogDto,
      });
      return successResponse(__('Updated successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async delete(id: string): Promise<ResponseModel> {
    try {
      const blog = await prisma_client.blog.findFirst({
        where: {
          uid: id,
        },
      });
      if (!blog) {
        throwNewError('Invalid blog id');
      }

      await prisma_client.blog.delete({
        where: {
          uid: id,
        },
      });
      return successResponse(__('Deleted successfully!'));
    } catch (e) {
      processException(e);
    }
  }
}
