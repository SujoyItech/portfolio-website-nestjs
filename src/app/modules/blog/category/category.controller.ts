import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../../../libs/auth/auth.gaurd';
import { CreateBlogCategoryDto, UpdateBlogCategoryDto } from '../dto/input.dto';
import { BlogCategoryService } from './category.service';
import { ResponseModel } from '../../../models/custom/common.response.model';
import { BlogCategory } from '../../../models/db/blog_category.model';

@UseGuards(JwtAuthGuard())
@Controller('blog-category')
export class BlogCategoryController {
  constructor(private readonly service: BlogCategoryService) {}

  @Get('')
  async findMany(): Promise<BlogCategory[]> {
    return await this.service.findMany();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<BlogCategory | null> {
    return await this.service.findOne(id);
  }

  @Post()
  async create(
    @Body() createBlogCategoryDto: CreateBlogCategoryDto,
  ): Promise<ResponseModel> {
    return this.service.create(createBlogCategoryDto);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateBlogCategoryDto: UpdateBlogCategoryDto,
  ): Promise<ResponseModel> {
    return this.service.update(id, updateBlogCategoryDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<ResponseModel> {
    return this.service.delete(id);
  }
}
