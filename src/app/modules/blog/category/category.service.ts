import { CreateBlogCategoryDto, UpdateBlogCategoryDto } from '../dto/input.dto';
import {
  __,
  prisma_client,
  processException,
  successResponse,
} from '../../../helpers/functions';
import { ResponseModel } from '../../../models/custom/common.response.model';
import { BlogCategory } from '../../../models/db/blog_category.model';
import { throwNewError } from '../../../exception/exception_filter';
import { STATUS_ACTIVE } from '../../../helpers/coreconstant';

export class BlogCategoryService {
  async findMany(): Promise<BlogCategory[]> {
    try {
      return await prisma_client.blogCategory.findMany({
        where: {
          status: STATUS_ACTIVE,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async findOne(id: string): Promise<BlogCategory | null> {
    try {
      return await prisma_client.blogCategory.findFirst({
        where: {
          uid: id,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async create(
    createBlogCategoryDto: CreateBlogCategoryDto,
  ): Promise<ResponseModel> {
    try {
      await prisma_client.blogCategory.create({
        data: createBlogCategoryDto,
      });
      return successResponse(__('Created successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async update(
    uid: string,
    updateBlogCategoryDto: UpdateBlogCategoryDto,
  ): Promise<ResponseModel> {
    try {
      const blogCategory = await prisma_client.blogCategory.findFirst({
        where: {
          uid: uid,
        },
      });
      if (!blogCategory) {
        throwNewError('Invalid uid!');
      }

      await prisma_client.blogCategory.update({
        where: {
          id: blogCategory.id,
        },
        data: updateBlogCategoryDto,
      });
      return successResponse(__('Updated successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async delete(id: string): Promise<ResponseModel> {
    try {
      const blogCategory = await prisma_client.blogCategory.findFirst({
        where: {
          uid: id,
        },
      });
      if (!blogCategory) {
        throwNewError('Invalid id!');
      }

      await prisma_client.blogCategory.delete({
        where: {
          id: blogCategory.id,
        },
      });
      return successResponse(__('Deleted successfully!'));
    } catch (e) {
      processException(e);
    }
  }
}
