import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../../libs/auth/auth.gaurd';
import { ResponseModel } from '../../models/custom/common.response.model';
import { Blog } from '../../models/db/blog.model';
import { BlogService } from './blog.service';
import { CreateBlogDto, UpdateBlogDto } from './dto/input.dto';

@UseGuards(JwtAuthGuard())
@Controller('skill')
export class BlogController {
  constructor(private readonly service: BlogService) {}

  @Get()
  async findAll(): Promise<Blog[]> {
    return await this.service.findMany();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Blog | null> {
    return await this.service.findOne(id);
  }

  @Post()
  async create(@Body() createBlogDto: CreateBlogDto): Promise<ResponseModel> {
    return await this.service.create(createBlogDto);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateBlogDto: UpdateBlogDto,
  ): Promise<ResponseModel> {
    return await this.service.update(id, updateBlogDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<ResponseModel> {
    return await this.service.delete(id);
  }
}
