import { IsNotEmpty } from 'class-validator';
import { __ } from '../../../helpers/functions';

export class ProfileBaseInputDto {
  @IsNotEmpty({
    message: () => __('Name is required'),
  })
  full_name: string;
  greeting_text?: string;
  designation?: string;
  description?: string;
  image?: string;
}

export class CreateProfileInputDto extends ProfileBaseInputDto {}
export class UpdateProfileInputDto extends ProfileBaseInputDto {
  @IsNotEmpty({
    message: () => __('Uid is required'),
  })
  uid: string;
}
