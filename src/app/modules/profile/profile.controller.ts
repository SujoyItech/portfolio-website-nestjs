import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../../libs/auth/auth.gaurd';
import { CreateProfileInputDto, UpdateProfileInputDto } from './dto/input.dto';
import { ProfileService } from './profile.service';
import { ResponseModel } from '../../models/custom/common.response.model';
import { Profile } from '../../models/db/profile.model';
import { Request } from 'express';

@UseGuards(JwtAuthGuard())
@Controller('profile')
export class ProfileController {
  constructor(private readonly service: ProfileService) {}

  @Get(':uid')
  async profile(@Param() params: any): Promise<Profile | null> {
    return await this.service.profile(params.uid);
  }

  @Post('create')
  async create(
    @Req() req: Request,
    @Body() data: CreateProfileInputDto,
  ): Promise<ResponseModel> {
    return this.service.create(req, data);
  }

  @Post('update')
  async update(
    @Req() req: Request,
    @Body() data: UpdateProfileInputDto,
  ): Promise<ResponseModel> {
    return this.service.update(req, data);
  }
}
