import { CreateProfileInputDto, UpdateProfileInputDto } from './dto/input.dto';
import {
  __,
  deleteImage,
  prisma_client,
  processException,
  successResponse,
  uploadImage,
} from '../../helpers/functions';
import { ResponseModel } from '../../models/custom/common.response.model';
import { Profile } from '../../models/db/profile.model';
import { throwNewError } from '../../exception/exception_filter';
import { Request } from 'express';

export class ProfileService {
  async profile(uid: string): Promise<Profile | null> {
    try {
      return await prisma_client.profile.findFirst({
        where: {
          uid: uid,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async create(
    req: Request,
    data: CreateProfileInputDto,
  ): Promise<ResponseModel> {
    try {
      const files = req.files as Express.Multer.File[];
      if (files && files.length > 0) {
        for (const file of files) {
          if ((file.fieldname = 'image')) {
            const imageUrl = await uploadImage(file, `profile`);
            data.image = imageUrl;
          }
        }
      }
      await prisma_client.profile.create({
        data: data,
      });
      return successResponse(__('Created successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async update(
    req: Request,
    data: UpdateProfileInputDto,
  ): Promise<ResponseModel> {
    try {
      const { uid } = data;
      const profile = await prisma_client.profile.findFirst({
        where: {
          uid: uid,
        },
      });
      if (!profile) {
        throwNewError('Invalid profile uid');
      }

      let image = profile.image;

      const files = req.files as Express.Multer.File[];
      if (files.length > 0) {
        for (const file of files) {
          if ((file.fieldname = 'image')) {
            const imageUrl = await uploadImage(file, `profile`);
            if (imageUrl) {
              image = imageUrl;
              await deleteImage(profile.image);
            }
          }
        }
      }

      data.image = image;

      await prisma_client.profile.update({
        where: {
          uid: uid,
        },
        data: data,
      });
      return successResponse(__('Updated successfully!'));
    } catch (e) {
      processException(e);
    }
  }
}
