import { IsNotEmpty } from 'class-validator';
import { __ } from '../../../helpers/functions';

export class MyServicesBaseDto {
  @IsNotEmpty({
    message: () => __('Title is required'),
  })
  title: string;
  logo?: string;
  sub_title?: string;
  description?: string;
  is_active?: number;
  status?: number;
}

export class CreateMyServicesDto extends MyServicesBaseDto {}
export class UpdateMyServicesDto extends MyServicesBaseDto {}
