import { Module } from '@nestjs/common';
import { MyServiceController } from './my_service.controller';
import { MyServiceService } from './my_service.service';

@Module({
  imports: [],
  controllers: [MyServiceController],
  providers: [MyServiceService],
  exports: [],
})
export class MyServiceModule {}
