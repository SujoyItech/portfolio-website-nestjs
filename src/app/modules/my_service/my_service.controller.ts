import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { JwtAuthGuard } from '../../../libs/auth/auth.gaurd';
import { ResponseModel } from '../../models/custom/common.response.model';
import { MyServices } from '../../models/db/my_service.model';
import { CreateMyServicesDto, UpdateMyServicesDto } from './dto/input.dto';
import { MyServiceService } from './my_service.service';

@UseGuards(JwtAuthGuard())
@Controller('my_services')
export class MyServiceController {
  constructor(private readonly service: MyServiceService) {}

  @Get()
  async findAll(): Promise<MyServices[]> {
    return await this.service.findMany();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<MyServices | null> {
    return await this.service.findOne(id);
  }

  @Post()
  async create(
    @Req() req: Request,
    @Body() createMyServicesDto: CreateMyServicesDto,
  ): Promise<ResponseModel> {
    return await this.service.create(req, createMyServicesDto);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Req() req: Request,
    @Body() updateMyServicesDto: UpdateMyServicesDto,
  ): Promise<ResponseModel> {
    return await this.service.update(id, req, updateMyServicesDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<ResponseModel> {
    return await this.service.delete(id);
  }
}
