import { CreateMyServicesDto, UpdateMyServicesDto } from './dto/input.dto';
import {
  __,
  deleteImage,
  prisma_client,
  processException,
  successResponse,
  uploadImage,
} from '../../helpers/functions';
import { ResponseModel } from '../../models/custom/common.response.model';
import { MyServices } from '../../models/db/my_service.model';
import { throwNewError } from '../../exception/exception_filter';
import { STATUS_ACTIVE } from '../../helpers/coreconstant';
import { Request } from 'express';

export class MyServiceService {
  async findMany(): Promise<MyServices[]> {
    try {
      return await prisma_client.myServices.findMany({
        where: {
          status: STATUS_ACTIVE,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async findOne(uid: string): Promise<MyServices | null> {
    try {
      return await prisma_client.myServices.findFirst({
        where: {
          uid: uid,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async create(
    req: Request,
    createMyServiceDto: CreateMyServicesDto,
  ): Promise<ResponseModel> {
    try {
      const files = req.files as Express.Multer.File[];
      if (files && files.length > 0) {
        for (const file of files) {
          if ((file.fieldname = 'logo')) {
            const logoUrl = await uploadImage(file, `my_services`);
            createMyServiceDto.logo = logoUrl;
          }
        }
      }
      await prisma_client.myServices.create({
        data: createMyServiceDto,
      });
      return successResponse(__('Created successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async update(
    id: string,
    req: Request,
    updateMyServiceDto: UpdateMyServicesDto,
  ): Promise<ResponseModel> {
    try {
      const myServices = await prisma_client.myServices.findFirst({
        where: {
          uid: id,
        },
      });
      if (!myServices) {
        throwNewError('Invalid id');
      }

      let logo = myServices.logo;

      const files = req.files as Express.Multer.File[];
      if (files.length > 0) {
        for (const file of files) {
          if ((file.fieldname = 'logo')) {
            const logoUrl = await uploadImage(file, `logo`);
            if (logoUrl) {
              logo = logoUrl;
              await deleteImage(myServices.logo);
            }
          }
        }
      }

      updateMyServiceDto.logo = logo;

      await prisma_client.myServices.update({
        where: {
          uid: id,
        },
        data: updateMyServiceDto,
      });
      return successResponse(__('Updated successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async delete(id: string): Promise<ResponseModel> {
    try {
      const myServices = await prisma_client.myServices.findFirst({
        where: {
          uid: id,
        },
      });
      if (!myServices) {
        throwNewError('Invalid id');
      }

      await prisma_client.myServices.delete({
        where: {
          uid: id,
        },
      });
      return successResponse(__('Deleted successfully!'));
    } catch (e) {
      processException(e);
    }
  }
}
