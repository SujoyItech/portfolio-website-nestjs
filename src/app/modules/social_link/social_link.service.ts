import {
  CreateSocialLinkInputDto,
  UpdateSocialLinkInputDto,
} from './dto/input.dto';
import {
  __,
  prisma_client,
  processException,
  successResponse,
} from '../../helpers/functions';
import { ResponseModel } from '../../models/custom/common.response.model';
import { throwNewError } from '../../exception/exception_filter';
import { SocialLink } from '../../models/db/social_links.model';

export class SocialLinkService {
  async socialLink(uid: string): Promise<SocialLink | null> {
    try {
      return await prisma_client.socialLink.findFirst({
        where: {
          uid: uid,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async create(data: CreateSocialLinkInputDto): Promise<ResponseModel> {
    try {
      await prisma_client.socialLink.create({
        data: data,
      });
      return successResponse(__('Created successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async update(data: UpdateSocialLinkInputDto): Promise<ResponseModel> {
    try {
      const { uid } = data;
      const socialLink = await prisma_client.socialLink.findFirst({
        where: {
          uid: uid,
        },
      });
      if (!socialLink) {
        throwNewError('Invalid social link uid');
      }

      await prisma_client.socialLink.update({
        where: {
          uid: uid,
        },
        data: data,
      });
      return successResponse(__('Updated successfully!'));
    } catch (e) {
      processException(e);
    }
  }
}
