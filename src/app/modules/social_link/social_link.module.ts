import { Module } from '@nestjs/common';
import { SocialLinkController } from './social_link.controller';
import { SocialLinkService } from './social_link.service';

@Module({
  imports: [],
  controllers: [SocialLinkController],
  providers: [SocialLinkService],
  exports: [],
})
export class SocialLinkModule {}
