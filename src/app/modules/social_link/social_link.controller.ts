import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../../../libs/auth/auth.gaurd';
import { ResponseModel } from '../../models/custom/common.response.model';
import { SocialLink } from '../../models/db/social_links.model';
import {
  CreateSocialLinkInputDto,
  UpdateSocialLinkInputDto,
} from './dto/input.dto';
import { SocialLinkService } from './social_link.service';

@UseGuards(JwtAuthGuard())
@Controller('social-links')
export class SocialLinkController {
  constructor(private readonly service: SocialLinkService) {}

  @Get(':uid')
  async socialLink(@Param() params: any): Promise<SocialLink | null> {
    return await this.service.socialLink(params.uid);
  }

  @Post('create')
  async create(@Body() data: CreateSocialLinkInputDto): Promise<ResponseModel> {
    return this.service.create(data);
  }

  @Post('update')
  async update(@Body() data: UpdateSocialLinkInputDto): Promise<ResponseModel> {
    return this.service.update(data);
  }
}
