import { IsNotEmpty, IsString } from 'class-validator';
import { __ } from '../../../helpers/functions';

export class SocialLinkBaseInputDto {
  @IsString()
  linked_in?: string;
  github?: string;
  gitlab?: string;
  bitbucket?: string;
  drible?: string;
  stack_overflow?: string;
  twitte?: string;
  skype?: string;
}

export class CreateSocialLinkInputDto extends SocialLinkBaseInputDto {}
export class UpdateSocialLinkInputDto extends SocialLinkBaseInputDto {
  @IsNotEmpty({
    message: () => __('Uid is required'),
  })
  uid: string;
}
