import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { JwtAuthGuard } from '../../../libs/auth/auth.gaurd';
import { ResponseModel } from '../../models/custom/common.response.model';
import { Testimonial } from '../../models/db/testimonial.model';
import { CreateTestimonialDto, UpdateTestimonialDto } from './dto/input.dto';
import { TestimonialService } from './testimonial.service';

@UseGuards(JwtAuthGuard())
@Controller('testimonial')
export class TestimonialController {
  constructor(private readonly service: TestimonialService) {}

  @Get()
  async findAll(): Promise<Testimonial[]> {
    return await this.service.findMany();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Testimonial | null> {
    return await this.service.findOne(id);
  }

  @Post()
  async create(
    @Req() req: Request,
    @Body() createTestimonialDto: CreateTestimonialDto,
  ): Promise<ResponseModel> {
    return await this.service.create(req, createTestimonialDto);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Req() req: Request,
    @Body() updateTestimonialDto: UpdateTestimonialDto,
  ): Promise<ResponseModel> {
    return await this.service.update(id, req, updateTestimonialDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<ResponseModel> {
    return await this.service.delete(id);
  }
}
