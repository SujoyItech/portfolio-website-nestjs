import { IsNotEmpty } from 'class-validator';
import { __ } from '../../../helpers/functions';

export class TestimonialBaseDto {
  @IsNotEmpty({
    message: () => __('Name is required'),
  })
  name: string;
  address?: string;
  designation?: string;
  image?: string;
  rating?: number;
  description?: string;
  status?: number;
}

export class CreateTestimonialDto extends TestimonialBaseDto {}
export class UpdateTestimonialDto extends TestimonialBaseDto {}
