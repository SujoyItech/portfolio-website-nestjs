import { CreateTestimonialDto, UpdateTestimonialDto } from './dto/input.dto';
import {
  __,
  deleteImage,
  prisma_client,
  processException,
  successResponse,
  uploadImage,
} from '../../helpers/functions';
import { ResponseModel } from '../../models/custom/common.response.model';
import { Testimonial } from '../../models/db/testimonial.model';
import { throwNewError } from '../../exception/exception_filter';
import { STATUS_ACTIVE } from '../../helpers/coreconstant';
import { Request } from 'express';

export class TestimonialService {
  async findMany(): Promise<Testimonial[]> {
    try {
      return await prisma_client.testimonial.findMany({
        where: {
          status: STATUS_ACTIVE,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async findOne(uid: string): Promise<Testimonial | null> {
    try {
      return await prisma_client.testimonial.findFirst({
        where: {
          uid: uid,
        },
      });
    } catch (e) {
      processException(e);
    }
  }

  async create(
    req: Request,
    createTestimonialDto: CreateTestimonialDto,
  ): Promise<ResponseModel> {
    try {
      const files = req.files as Express.Multer.File[];
      if (files && files.length > 0) {
        for (const file of files) {
          if ((file.fieldname = 'image')) {
            const imageUrl = await uploadImage(file, `testimonial`);
            createTestimonialDto.image = imageUrl;
          }
        }
      }
      await prisma_client.testimonial.create({
        data: createTestimonialDto,
      });
      return successResponse(__('Created successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async update(
    id: string,
    req: Request,
    updateTestimonialDto: UpdateTestimonialDto,
  ): Promise<ResponseModel> {
    try {
      const testimonial = await prisma_client.testimonial.findFirst({
        where: {
          uid: id,
        },
      });
      if (!testimonial) {
        throwNewError('Invalid testimonial id');
      }

      let image = testimonial.image;

      const files = req.files as Express.Multer.File[];
      if (files.length > 0) {
        for (const file of files) {
          if ((file.fieldname = 'image')) {
            const imageUrl = await uploadImage(file, `image`);
            if (imageUrl) {
              image = imageUrl;
              await deleteImage(testimonial.image);
            }
          }
        }
      }

      updateTestimonialDto.image = image;

      await prisma_client.testimonial.update({
        where: {
          uid: id,
        },
        data: updateTestimonialDto,
      });
      return successResponse(__('Updated successfully!'));
    } catch (e) {
      processException(e);
    }
  }

  async delete(id: string): Promise<ResponseModel> {
    try {
      const testimonial = await prisma_client.testimonial.findFirst({
        where: {
          uid: id,
        },
      });
      if (!testimonial) {
        throwNewError('Invalid testimonial id');
      }

      await prisma_client.testimonial.delete({
        where: {
          uid: id,
        },
      });
      return successResponse(__('Deleted successfully!'));
    } catch (e) {
      processException(e);
    }
  }
}
