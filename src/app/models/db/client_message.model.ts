import { BaseModelHiddenId } from '../../../libs/model/base.model';

export class ClientMessage extends BaseModelHiddenId {
  uid: string;
  name?: string;
  email?: string;
  message?: string;
  status?: number;
}
