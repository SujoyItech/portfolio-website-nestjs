import { BaseModelHiddenId } from '../../../libs/model/base.model';

export class BlogTag extends BaseModelHiddenId {
  name: string;
  status: number;
}
