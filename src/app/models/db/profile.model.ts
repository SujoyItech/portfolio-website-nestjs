export class Profile {
  uid: string;
  full_name?: string;
  greeting_text?: string;
  designation?: string;
  description: string;
}
