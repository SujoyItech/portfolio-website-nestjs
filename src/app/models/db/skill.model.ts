import { BaseModelHiddenId } from '../../../libs/model/base.model';
import { SkillCategory } from './skill_category.model';

export class Skill extends BaseModelHiddenId {
  title: string;
  description?: string;
  organization?: string;
  date_range?: string;
  percent?: number;
  status?: number;
  category?: SkillCategory;
}
