export class AboutMe {
  uid: string;
  title?: string;
  description?: string;
  name?: string;
  designation?: string;
  email?: string;
  secondary_email?: string;
  phone?: string;
  secondary_phone?: string;
  address?: string;
  secondary_address?: string;
  nationality?: string;
  language?: string;
  secondary_language?: string;
  image?: string;
}
