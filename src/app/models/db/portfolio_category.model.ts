import { BaseModelHiddenId } from '../../../libs/model/base.model';
import { Portfolio } from './portfolio.model';

export class PortfolioCategory extends BaseModelHiddenId {
  uid: string;
  title: string;
  status: number;
  portfolios?: Portfolio;
}
