import { BaseModelHiddenId } from '../../../libs/model/base.model';

export class MyServices extends BaseModelHiddenId {
  uid?: string;
  title?: string;
  logo?: string;
  sub_title?: string;
  description?: string;
  is_active?: number;
  status?: number;
}
