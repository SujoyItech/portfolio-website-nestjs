import { BaseModelHiddenId } from '../../../libs/model/base.model';

export class Achievement extends BaseModelHiddenId {
  title: string;
  sub_title?: string;
  logo?: string;
  total_count?: number;
  status?: number;
}
