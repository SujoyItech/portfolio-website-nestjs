import { BaseModelHiddenId } from '../../../libs/model/base.model';
import { Skill } from './skill.model';
export class SkillCategory extends BaseModelHiddenId {
  uid: string;
  title: string;
  status: number;
  skills?: Skill;
}
