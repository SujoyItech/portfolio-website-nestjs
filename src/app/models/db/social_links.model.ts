export class SocialLink {
  uid: string;
  linked_in?: string;
  github?: string;
  gitlab?: string;
  bitbucket?: string;
  drible?: string;
  stack_overflow?: string;
  twitte?: string;
  skype?: string;
}
