import { BaseModelHiddenId } from '../../../libs/model/base.model';
import { PortfolioCategory } from './portfolio_category.model';

export class Portfolio extends BaseModelHiddenId {
  title: string;
  sub_title?: string;
  description?: string;
  project_link?: string;
  image?: string;
  status?: number;
  category?: PortfolioCategory;
}
