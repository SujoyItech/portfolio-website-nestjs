import { BaseModelHiddenId } from '../../../libs/model/base.model';

export class Subscription extends BaseModelHiddenId {
  email: string;
  status: number;
}
