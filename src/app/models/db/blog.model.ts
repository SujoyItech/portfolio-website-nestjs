import { BaseModelHiddenId } from '../../../libs/model/base.model';

export class Blog extends BaseModelHiddenId {
  uid: string;
  title?: string;
  slug?: string;
  image?: string;
  short_description?: string;
  long_description?: string;
  tags?: string;
  status?: number;
}
