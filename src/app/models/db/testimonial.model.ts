import { BaseModelHiddenId } from '../../../libs/model/base.model';

export class Testimonial extends BaseModelHiddenId {
  uid: string;
  name?: string;
  address?: string;
  designation?: string;
  image?: string;
  rating?: number;
  description?: string;
  status?: number;
}
