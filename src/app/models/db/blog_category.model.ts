import { BaseModelHiddenId } from '../../../libs/model/base.model';

export class BlogCategory extends BaseModelHiddenId {
  uid: string;
  name: string;
  status: number;
}
