export enum SETTINGS_VALUE_TYPE {
  TEXT = 1,
  MEDIA_URL = 2,
}

export enum SETTINGS_GROUP {
  APPLICATION = 'application_settings',
  API = 'api_settings',
  GENERAL = 'general_settings',
  EMAIL = 'email_settings',
  LOGO = 'logo_settings',
  SOCIAL = 'social_settings',
}

export enum SETTINGS_SLUG {
  //application settings

  //general_settings
  APPLICATION_TITLE = 'application_title',
  CONTRACT_EMAIL = 'contract_email',
  CONTRACT_PHONE = 'contract_phone',
  ADDRESS = 'address',
  COPY_RIGHT = 'copy_right_text',

  //api_settings
  AWS_S3_BUCKET = 'aws_s3_bucket',
  AWS_S3_ACCESS_KEY = 'aws_s3_access_key',
  AWS_S3_KEY_SECRET = 'aws_s3_key_secret',
  AWS_S3_REGION = 'aws_s3_region',

  //email_settings
  MAIL_DIRIVER = 'mail_driver',
  MAIL_HOST = 'mail_host',
  MAIL_PORT = 'mail_port',
  MAIL_USERNAME = 'mail_username',
  MAIL_PASSWORD = 'mail_password',
  MAIL_ENCRYPTION = 'mail_encryption',
  MAIL_FROM_ADDRESS = 'mail_from_address',
  MAIL_FROM_NAME = 'mail_from_name',

  //logo_settings
  APP_LOGO_LARGE = 'app_logo_large',
  APP_LOGO_SMALL = 'app_logo_small',
  FAVICON_LOGO = 'favicon_logo',
  AUTH_LEFT_IMAGE = 'auth_left_image',

  //social_settings
  FACEBOOK_LINK = 'facebook_link',
  TWITTER_LINK = 'twitter_link',
  INSTAGRAM_LINK = 'instagram_link',
  DISCORD_LINK = 'discord_link',
  WHATSAPP_LINK = 'whatsapp_link',
  LINKEDIN_LINK = 'linkedin_link',
  //
}
