import { Module } from '@nestjs/common';
import { AuthModule } from './modules/auth/auth.module';
import { UserModule } from './modules/user/user.module';
import { ProfileModule } from './modules/profile/profile.module';
import { AboutMeModule } from './modules/about_me/about_me.module';
import { SocialLinkModule } from './modules/social_link/social_link.module';
import { SkillModule } from './modules/skills/skills.module';
import { AchievementModule } from './modules/achievement/achievement.module';
import { TestimonialModule } from './modules/testimonial/testimonial.module';
import { MyServiceModule } from './modules/my_service/my_service.module';
import { BlogModule } from './modules/blog/blog.module';

@Module({
  imports: [
    AuthModule,
    UserModule,
    ProfileModule,
    AboutMeModule,
    SocialLinkModule,
    ProfileModule,
    SkillModule,
    AchievementModule,
    TestimonialModule,
    MyServiceModule,
    BlogModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
