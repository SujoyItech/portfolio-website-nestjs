import { BadRequestException, Injectable } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import { ReadStream } from 'fs';

import { FileUtil } from './file_util';
import {
  __,
  clearBothEndSlash,
  errorResponse,
  getApiErrMsg,
  getSettingsGroup,
  successResponse,
} from '../../app/helpers/functions';
import { SETTINGS_GROUP, SETTINGS_SLUG } from '../../app/helpers/slugconstants';

@Injectable()
export class S3Service {
  protected s3: AWS.S3;
  protected bucket: string;
  protected region: string;
  async init() {
    const settings = await getSettingsGroup([SETTINGS_GROUP.API]);

    this.bucket =
      settings[SETTINGS_SLUG.AWS_S3_BUCKET] ?? process.env.AWS_S3_BUCKET;
    this.region =
      settings[SETTINGS_SLUG.AWS_S3_REGION] ?? process.env.AWS_S3_REGION;
    this.s3 = new AWS.S3({
      accessKeyId:
        settings[SETTINGS_SLUG.AWS_S3_ACCESS_KEY] ??
        process.env.AWS_S3_ACCESS_KEY,
      secretAccessKey:
        settings[SETTINGS_SLUG.AWS_S3_KEY_SECRET] ??
        process.env.AWS_S3_KEY_SECRET,
    });
  }

  async uploadFile(
    upload: Express.Multer.File,
    dir: string,
    validFileTypes?: string[],
    maxSizeInByte?: number,
  ) {
    await this.init();
    const file = new FileUtil(upload, maxSizeInByte);
    if (!(await file.isValid(validFileTypes)))
      throw new BadRequestException(
        errorResponse(
          __('Invalid file type. Valid types are: ') + validFileTypes,
        ),
      );

    await this.upload(
      file.readStream(),
      dir + '/' + file.hashName(),
      file.mimeType(),
    );
    return successResponse('');
  }

  async upload(
    file: ReadStream,
    name: string,
    mimetype: string,
    dir = '',
    bucket?: string,
    region?: string,
  ): Promise<string> {
    dir = clearBothEndSlash(dir);
    const params = {
      Bucket: bucket || this.bucket,
      Key: dir ? `${dir}/${name}` : name,
      Body: file,
      ACL: 'public-read',
      ContentType: mimetype,
      ContentDisposition: 'inline',
      CreateBucketConfiguration: {
        LocationConstraint: region || this.region,
      },
    };
    const s3Response = await this.s3.upload(params).promise();
    return s3Response.Location;
  }

  async delete(filepath: string, bucket?: string): Promise<boolean> {
    try {
      const key = filepath.split('.amazonaws.com/')[1];
      await this.init();
      const params = {
        Bucket: bucket || this.bucket,
        Key: key,
      };
      // await this.s3.headObject(params).promise();
      await this.s3.deleteObject(params).promise();
      return true;
    } catch (e) {
      console.error(getApiErrMsg(e));
      return false;
    }
  }
}
