import { Global, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { FilesystemService } from './filesystem.service';
import { NestFilesystemModule } from '@filesystem/nestjs';
import { IFilesystemModuleOptions } from '@filesystem/nestjs/interfaces/file-system-module-options';
import { S3Service } from './s3_service';

@Global()
@Module({
  imports: [
    ConfigModule,
    NestFilesystemModule.registerAsync({
      availableDisks: ['local'],
      useFactory: (configService: ConfigService) => {
        return configService.get<IFilesystemModuleOptions>('filesystem');
      },
      inject: [ConfigService],
    }),
  ],
  controllers: [],
  providers: [FilesystemService, S3Service],
  exports: [FilesystemService, S3Service],
})
export class FilesystemModule {}
