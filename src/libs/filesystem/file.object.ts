class FileVariant {
  type: string;
  url: string;
}

export class FileObject {
  name: string;
  type: string;
  url: string;
  variants?: Array<FileVariant>;
}
