import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectFilesystem } from '@filesystem/nestjs';
import { IFilesystemAdapter } from '@filesystem/core/interfaces';
import { ConfigService } from '@nestjs/config';
import { IFilesystemModuleOptions } from '@filesystem/nestjs/interfaces/file-system-module-options';
import { FileUtil } from './file_util';
import { FileObject } from './file.object';
import { join, resolve } from 'path';
import { readdir } from 'fs/promises';

import { S3Service } from './s3_service';
import { createReadStream, existsSync, readFileSync } from 'fs';
import { FILESYSTEM_DISK } from '../../app/helpers/coreconstant';
import {
  __,
  clearBothEndSlash,
  clearTrailingSlash,
  errorResponse,
} from '../../app/helpers/functions';
import { User } from '../../app/models/db/user.model';

@Injectable()
export class FilesystemService {
  private disk: FILESYSTEM_DISK =
    <any>process.env.FILESYSTEM_DISK || FILESYSTEM_DISK.LOCAL;
  constructor(
    private readonly configService: ConfigService,
    @InjectFilesystem('local') private readonly localDisk: IFilesystemAdapter,
    private readonly s3: S3Service,
  ) {}

  private setDisk(disk: FILESYSTEM_DISK) {
    this.disk = disk;
  }

  private resetDisk() {
    this.disk = <any>process.env.FILESYSTEM_DISK || FILESYSTEM_DISK.LOCAL;
  }

  getDisk(disk?: FILESYSTEM_DISK): FILESYSTEM_DISK {
    return disk || this.disk;
  }

  async listFile(user: User): Promise<FileObject[]> {
    const baseDir = `user-${user.id}`;
    const directory = resolve(`public/storage/${baseDir}`);
    try {
      const files = await readdir(directory);
      return files
        .filter((item) => {
          const reg = /[\w,\s-]+\.[A-Za-z0-9]{3}/g;
          return reg.test(item);
        })
        .map((item): FileObject => {
          const type = FileUtil.type(item);
          return {
            name: item,
            type: type,
            url: this.url(baseDir + '/' + item),
            variants:
              type == 'image'
                ? Object.keys(FileUtil.imageSizes).map((sizeKey) => {
                    return {
                      type: sizeKey,
                      url: this.url(baseDir + '/' + sizeKey + '/' + item),
                    };
                  })
                : null,
          };
        });
    } catch (e) {
      console.error(e.stack);
      return [];
    }
  }

  async upload(
    upload: Express.Multer.File,
    dir: string,
    validFileTypes?: string[],
    maxSizeInByte?: number,
    disk?: FILESYSTEM_DISK,
  ): Promise<FileObject> {
    dir = clearBothEndSlash(dir);
    const file = new FileUtil(upload, maxSizeInByte);
    if (!(await file.isValid(validFileTypes)))
      throw new BadRequestException(
        errorResponse(
          __('Invalid file type. Valid types are: ') + validFileTypes,
        ),
      );
    //if (file.isImage()) return await this.resizeAndSave(file, dir, disk);

    const name = file.hashName();
    const url = await this.writeFile(file, dir, name, disk);
    return {
      name: name,
      type: file.type(),
      url: url,
    };
  }

  async writeFile(
    file: FileUtil,
    dir: string,
    name: string,
    disk: FILESYSTEM_DISK,
  ): Promise<string> {
    const diskName = this.getDisk(disk);
    if (diskName == FILESYSTEM_DISK.AWS_S3) {
      await this.s3.init();
      return await this.s3.upload(
        file.readStream(),
        name,
        file.mimeType(),
        dir,
      );
    } else {
      const fullpath = dir ? `${dir}/${name}` : name;
      await this.localDisk.writeStream(fullpath, file.readStream());
      return `storage/${fullpath}`;
    }
  }

  url(filepath: string): string {
    if (!filepath) return null;
    const isLocalDisk = this.detetcLocalDiskFromFilePath(filepath);
    const filesystemConfig =
      this.configService.get<IFilesystemModuleOptions>('filesystem');
    if (isLocalDisk) {
      const diskConfig = filesystemConfig.disks[FILESYSTEM_DISK.LOCAL];
      return `${clearTrailingSlash(
        diskConfig.url.trimEnd(),
      )}/${filepath.trimStart()}`;
    } else {
      return filepath;
    }
  }

  detetcLocalDiskFromFilePath(filepath: string): boolean {
    const proto = filepath.split('://')[0];
    if (proto == 'http' || proto == 'https') {
      return false;
    } else {
      return true;
    }
  }

  async deleteFile(filepath: string, disk?: FILESYSTEM_DISK): Promise<boolean> {
    if (!filepath) return true;
    const diskName = this.getDisk(disk);
    if (diskName == FILESYSTEM_DISK.AWS_S3) {
      await this.s3.delete(filepath);
    } else if (diskName == FILESYSTEM_DISK.LOCAL) {
      filepath = filepath.split('storage/').pop();
      await this.localDisk.delete(filepath);
    }
    return true;
  }

  async downloadFileByStream(filePath: string) {
    if (!filePath) {
      throw new BadRequestException(errorResponse(__('File path is required')));
    }
    if (!existsSync(filePath)) {
      throw new BadRequestException(errorResponse(__('File not exists!')));
    }
    return createReadStream(join(process.cwd(), filePath));
  }

  async downloadFileByBuffer(filePath: string) {
    if (!filePath) {
      throw new BadRequestException(errorResponse(__('File path is required')));
    }
    if (!existsSync(filePath)) {
      throw new BadRequestException(errorResponse(__('File not exists!')));
    }
    return readFileSync(join(process.cwd(), filePath));
  }

  async adminUpload(upload: Express.Multer.File): Promise<FileObject> {
    const userDir = `admin`;
    const file = new FileUtil(upload);
    if (!file.isValid()) throw new Error('Invalid file');
    const name = file.hashName();
    await this.localDisk.writeStream(userDir + '/' + name, file.readStream());
    return {
      name: name,
      type: file.type(),
      url: this.url(userDir + '/' + name),
    };
  }

  async adminFileList(): Promise<Array<FileObject>> {
    const baseDir = `admin`;
    const directory = resolve(`public/storage/${baseDir}`);
    try {
      const files = await readdir(directory);
      return files
        .filter((item) => {
          const reg = /[\w,\s-]+\.[A-Za-z0-9]{3}/g;
          return reg.test(item);
        })
        .map((item): FileObject => {
          const type = FileUtil.type(item);
          return {
            name: item,
            type: type,
            url: this.url(baseDir + '/' + item),
            variants:
              type == 'image'
                ? Object.keys(FileUtil.imageSizes).map((sizeKey) => {
                    return {
                      type: sizeKey,
                      url: this.url(baseDir + '/' + sizeKey + '/' + item),
                    };
                  })
                : null,
          };
        });
    } catch (e) {
      console.error(e.stack);
      return [];
    }
  }
}
