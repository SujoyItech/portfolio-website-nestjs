import { PrismaClient } from '@prisma/client';
import { seedSeettings } from './seeds/settings.seeder';
import { seedUsers } from './seeds/user.seeder';

const prisma = new PrismaClient({ log: ['query'] });
//pass prisma from here to log or empty

async function main() {
  // await seedSeettings();
  await seedUsers(prisma);
  //
}

main()
  .catch((e) => {
    console.error(e.stack);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
