import { PrismaClient } from '@prisma/client';
export async function seedSeettings(prisma?: PrismaClient) {
  const prismaFromExternal = prisma;
  prisma = prisma ?? new PrismaClient();

  // await prisma.setting.createMany({
  //   skipDuplicates: true,
  // });

  if (!prismaFromExternal) await prisma.$disconnect();
}
