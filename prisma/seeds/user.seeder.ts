import { PrismaClient } from '@prisma/client';
import { hash } from 'bcryptjs';
import { USER_STATUS } from '../../src/app/helpers/coreconstant';
export async function seedUsers(prisma?: PrismaClient) {
  const prismaFromExternal = prisma;
  prisma = prisma ?? new PrismaClient();

  // system bot user
  await prisma.user.upsert({
    where: {
      usercode: 'sujoynath',
    },
    create: {
      usercode: 'sujoynath',
      name: 'Sujoy Nath',
      email: 'sujoy.swe@gmail.com',
      password: await hash('Pass.1234', 10),
      status: USER_STATUS.ACTIVE,
    },
    update: {},
  });

  if (!prismaFromExternal) await prisma.$disconnect();
}
// seedUsers();
